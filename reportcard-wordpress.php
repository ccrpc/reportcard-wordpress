<?php
/**
 * Plugin Name: Report Card WordPress Shortcodes
 * Description: Shortcodes for report card embedding
 *
 * Version: 0.1
 * Author: Champaign County Regional Planning Commission
 * Author URI: ccrpc.org
 */

function reportcard_register_scripts() {
  wp_register_script( 'reportcard-components',
    'https://reportcard.ccrpc.org/static/reportcard/components/reportcard.js',
    array(), null, true );
}
add_action( 'wp_enqueue_scripts', 'reportcard_register_scripts' );


function reportcard_performance_measure_shortcode( $raw_atts ) {
  wp_enqueue_script( 'reportcard-components' );

  $defaults = array(
    'url' => NULL,
  );
  $atts = shortcode_atts(
    $defaults, $raw_atts, 'reportcard_performance_measure' );

  if ( !$atts['url'] ) return;

  return '<reportcard-performance-measure-table url="' .
    esc_attr( $atts['url'] ) . '"></reportcard-performance-measure-table>';
}
add_shortcode( 'reportcard-performance-measure-table',
  'reportcard_performance_measure_shortcode' );

function reportcard_summary_shortcode( $raw_atts ) {
  wp_enqueue_script( 'reportcard-components' );

  $defaults = array(
    'url' => NULL,
  );
  $atts = shortcode_atts(
    $defaults, $raw_atts, 'reportcard_summary_shortcode' );

  if ( !$atts['url'] ) return;

  return '<reportcard-summary-table url="' .
    esc_attr( $atts['url'] ) . '"></reportcard-summary-table>';
}
add_shortcode( 'reportcard-summary-table', 'reportcard_summary_shortcode' );
