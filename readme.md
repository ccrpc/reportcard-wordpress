# Report Card WordPress Shortcodes

Shortcodes for report card embedding

## Credits
Report Card WordPress Shortcodes was developed by Matt Yoder for the
[Champaign County Regional Planning Commission][1].

## License
Report Card WordPress Shortcodes is available under the terms of the
[BSD 3-clause license][2].

[1]: https://ccrpc.org/
[2]: https://gitlab.com/ccrpc/reportcard-wordpress/blob/master/LICENSE.md
